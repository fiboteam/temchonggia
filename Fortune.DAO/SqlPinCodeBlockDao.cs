﻿using Vina.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vina.DAO
{
    public class SqlPinCodeBlockDao : SqlDaoBase<PinCodeBlock>
    {
        public SqlPinCodeBlockDao()
        {
            TableName = "tblPinCodeBlock";
            EntityIDName = "PinCodeBlockID";
            StoreProcedurePrefix = "spPinCodeBlock_";
        }

        public List<PinCodeBlock> GetList(long accountManagerId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms = { "@accountManagerId", accountManagerId };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int DeletePCB(long pincodeBlockId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "Delete";
                object[] parms = { "@pincodeBlockId", pincodeBlockId };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
