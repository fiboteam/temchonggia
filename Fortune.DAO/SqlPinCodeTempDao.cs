using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlPinCodeTempDao : SqlDaoBase<PinCodeTemp>
    {
        public SqlPinCodeTempDao()
        {
            TableName = "tblPinCodeTemp";
            EntityIDName = "PinCodeTempID";
            StoreProcedurePrefix = "spPinCodeTemp_";
        }

        public SqlPinCodeTempDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<PinCodeTemp> GetAllToExport(long createdById)
        {
            try
            {
                string sql = String.Format("select * from tblPinCodeTemp where CreatedByID = {0}",createdById);
                object[] parms = { };
                return DbAdapter1.ReadList(sql, Make, false, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int CountPinCodeByAccountID(long accountId)
        {
            try
            {
                string sql = "spPinCodeTemp_CountPinCodeByAccountID";
                object[] parms =
                {
                    "@accountId", accountId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public int DeletePinCodeByAccountID(long accountId)
        {
            try
            {
                string sql = "spPinCodeTemp_DeletePinCodeByAccountID";
                object[] parms =
                {
                    "@accountId", accountId
                };
                return DbAdapter1.ExecuteNonQuery(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }

}
