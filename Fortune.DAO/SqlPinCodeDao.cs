using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlPinCodeDao : SqlDaoBase<PinCodeObj>
    {
        public SqlPinCodeDao()
        {
            TableName = "tblPinCode";
            EntityIDName = "PinCodeID";
            StoreProcedurePrefix = "spPinCode_";
        }

        public List<PinCodeObj> GetWithFilter(long pageSize, long currentPage, string pinCode, string serial, int pinCodeStatus, long accountManagerID, DateTime fromDate,DateTime toDate, long pincodeblockId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = { "@pageSize", pageSize, "@currentPage", currentPage, "@pinCode", pinCode, "@serial", serial, "@pinCodeStatus", pinCodeStatus, "@accountManagerID", accountManagerID, "@fromDate", fromDate, "@toDate", toDate, "@pincodeblockId", pincodeblockId };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int CountPinCodeByAccountID(long accountId)
        {
            try
            {
                string sql = "spPinCode_CountPinCodeByAccountID";
                object[] parms =
                {
                    "@accountId", accountId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public int UpdatePinCodeStatus(int pinCodeStatus,long pinCodeId)
        {
            try
            {
                string sql = "spPinCode_UpdatePinCodeStatus";
                object[] parms =
                {
                    "@pinCodeStatus", pinCodeStatus, "@pinCodeId", pinCodeId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }
        

        public int CountPinCodeIsUsedByAccountID(long accountId)
        {
            try
            {
                string sql = "spPinCode_CountPinCodeIsUsedByAccountID";
                object[] parms =
                {
                    "@accountId", accountId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }
        }
        

        public List<PinCodeObj> GetList(long pageSize, long currentPage, string pinCode, string serial, int pinCodeStatus, long accountManagerID, DateTime fromDate, DateTime toDate)
        {
            object[] parms = { "@pageSize", pageSize, "@currentPage", currentPage, "@pinCode", pinCode, "@serial", serial, "@pinCodeStatus", pinCodeStatus, "@accountManagerID", accountManagerID, "@fromDate", fromDate, "@toDate", toDate };
            return DbAdapter1.ReadList("spPinCode_GetList", Make, true, parms);
        }
        public SqlPinCodeDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
        public List<PinCodeObj> SearchCard(string CardCode, int CardStatus = 0, string value = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "Search";
                object[] parms = { "@cardcode", CardCode };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return new List<PinCodeObj>();
            }
        }

        public List<PinCodeObj> Checking(string pincode,string serial)
        {
            try
            {
                string sql = StoreProcedurePrefix + "Checking";
                object[] parms = { "@pinCode", pincode, "@serial", serial };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return new List<PinCodeObj>();
            }
        }

        public PinCodeObj GetPinCode(string pincode,long accountManagerId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetPinCode";
                object[] parms = { "@pincode", pincode, "@accountManagerId", accountManagerId };
                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return new PinCodeObj();
            }
        }

        public int CheckPinCodeExists(string pinCode, long accountManagerId)
        {
            object[] parms = { "@pinCode", pinCode, "@accountManagerId", accountManagerId };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar("spPinCode_CheckPinCodeExists",true,parms));
        }
        public int DeletePC(long pcid)
        {
            string sql = "update tblPinCode set PinCodeStatus = 3 where PinCodeID = " + pcid;
            object[] parms = {  };
            return Convert.ToInt32(DbAdapter1.ExecuteNonQuery(sql, false, parms));
        }
    }
}
