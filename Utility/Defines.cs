﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    
    public enum TopUpStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        Pending = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Giao dịch thất bại
        /// </summary>
        Fail = 4
    }

    public enum IsSentSms : short
    {
        /// <summary>
        /// Tin vietel có mã card
        /// </summary>
        SentSuccess = 1,
        /// <summary>
        /// Tin send fail
        /// </summary>
        SentFail = 2
    }

    public enum ExcelSheetAt : short
    {
        Sheet1 = 0,
        Sheet2 = 1,
        Sheet3 = 2,
        Sheet4 = 3
    }

    public enum ExcelColunmAt : short
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4
    }

    public enum MoStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        WrongPinCode = 2,
        
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Giao dịch thất bại
        /// </summary>
        //
        WrongSyntax = 4,
        PinCodeIsUsed = 5,
        AccountIsDeactive = 6
    }

    public enum SubKeyStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        Active = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        DeActive = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum KeywordStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        Active = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        DeActive = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum PinCodeStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        Used = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum MtType : short
    {
        Success = 1,
        WrongSystax = 2,
        WrongPinCode = 3,
        PinCodeIsUsed = 4,
        SubKeyIsDeactive = 5
    }

    public enum PinCodeBlockStatus : short
    {
        Active = 1,
        DeActive = 2,
        Deleted = 3
    }

    public enum PinCodeListType : short
    {
        Success = 1,
        Duplicated = 2,
        WrongSyntax = 3
    }

    public enum ProductTypeStatus : short
    {
        Active = 1,
        DeActive = 2,
        Deleted = 100
    }
}
