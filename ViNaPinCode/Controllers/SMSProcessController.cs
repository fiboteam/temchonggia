﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace VinaVN.Controllers
{
    public class SMSProcessController : Controller
    {
        //
        // GET: /SMSProcess/
        private readonly SqlMtConfigDao mtConfigDao = new SqlMtConfigDao();
        private readonly SqlMoDao moDao = new SqlMoDao();
        private readonly SqlConfigDao configDao = new SqlConfigDao();
        private readonly SqlSubKeyDao subKeyDao = new SqlSubKeyDao();
        private readonly SqlPinCodeDao pinCodeDao = new SqlPinCodeDao();
        private readonly SqlMtDao mtDao = new SqlMtDao();
        private readonly SqlKeywordDao kwDao = new SqlKeywordDao();
        private readonly SqlProductTypeDao productTypeDao = new SqlProductTypeDao();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TestSMS()
        {
            return View(new TestMessageModel());
        }
        [HttpPost]
        public ActionResult TestSMS(TestMessageModel viewModel)
        {
            return MtProcess(viewModel.message, viewModel.phone, viewModel.service, viewModel.port, viewModel.guids,
                viewModel.main, viewModel.sub);
        }

        public ActionResult ReturnData(string xml)
        {
            Response.Flush();
            Response.Write(xml);
            Response.End();
            return View();
        }

        public ActionResult MtProcess(string message, string phone, string service, string port, string guid,
            string main, string sub)
        {

            try
            {
                string defaultWrongSyntaxMessage = "";//ConfigurationManager.AppSettings["DefaultWrongSyntaxMessage"].ToString();
                MtConfig conf = mtConfigDao.GetDefaultWrongSyntaxMessage();
                if(conf != null)
                {
                    defaultWrongSyntaxMessage = conf.MtMessage;
                }
                
                //string mainKey = ConfigurationManager.AppSettings["SMSMainKeyword"].ToString();
                string responeMessage = "";
                if (string.IsNullOrEmpty(phone))
                    return ReturnData("Du lieu ko hop le");
                bool status = false;
                message = message.ToUpper();
                List<string> messageArray = message.Trim().Split(' ').ToList();
                messageArray.RemoveAll(t => t == "");

                List<Keyword> keywordList = kwDao.GetAllKeyword();
                Keyword kw = keywordList.Where(m => m.KeywordValue == messageArray[0]).FirstOrDefault();

                Mo mo = new Mo();
                mo.PhoneNumber = phone;
                mo.MoMessage = message;

                mo.MainKey = messageArray[0];
                mo.GUID = guid;
                if (messageArray.Count() == 3)
                {
                    if (kw != null)
                    {
                        List<SubKey> listSubKey = subKeyDao.GetWithFilter(1000, 1, -1, kw.ID, "", 0);
                        SubKey sk = (listSubKey != null) ? (SubKey)listSubKey.Where(m => m.SubKeyValue == messageArray[1].ToUpper()).FirstOrDefault() : null;
                        //List<ProductType> productTypeList = productTypeDao.GetProductTypeByAccountID(kw.AccountManagerID);
                        //ProductType pt = (productTypeList != null) ? (ProductType)productTypeList.Where(m => m.ProductTypeKey == messageArray[1]).FirstOrDefault() : null;

                        if (kw != null && sk != null)
                        {
                            mo.MainKey = kw.KeywordValue;
                            mo.SubKeyID = sk.ID;
                            List<MtConfig> mtConfigList = mtConfigDao.GetConfigByAccountID(sk.AccountManagerID);
                            MtConfig mtSuccess = mtConfigList.Where(m => m.MtType == Utility.MtType.Success).FirstOrDefault();
                            MtConfig mtWrongSyntax = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongSystax).FirstOrDefault();
                            MtConfig mtWrongPinCode = mtConfigList.Where(m => m.MtType == Utility.MtType.WrongPinCode).FirstOrDefault();
                            MtConfig mtPinCodeIsUsed = mtConfigList.Where(m => m.MtType == Utility.MtType.PinCodeIsUsed).FirstOrDefault();
                            MtConfig mtSubKeyIsDeactive = mtConfigList.Where(m => m.MtType == Utility.MtType.SubKeyIsDeactive).FirstOrDefault();
                            PinCodeObj pinCode = pinCodeDao.GetPinCode(messageArray[2], sk.AccountManagerID);
                            if (kw.KeywordStatus == KeywordStatus.Active  && sk.SubKeyStatus == SubKeyStatus.Active)
                            {
                                if (pinCode != null)
                                {
                                    if (pinCode.ID > 0)
                                    {
                                        if (pinCode.PinCodeStatus == Utility.PinCodeStatus.JustCreated)
                                        {
                                            // Success
                                            status = true;
                                            mo.MoStatus = MoStatus.JustCreated;
                                            responeMessage = (mtSuccess != null) ? mtSuccess.MtMessage : "";
                                        }
                                        else
                                        {
                                            mo.MoStatus = MoStatus.PinCodeIsUsed;
                                            responeMessage = (mtPinCodeIsUsed != null) ? mtPinCodeIsUsed.MtMessage : "";
                                            //Pincode Is Used
                                        }
                                        mo.PinCodeID = pinCode.ID;
                                    }
                                    else
                                    {
                                        responeMessage = (mtWrongPinCode != null) ? mtWrongPinCode.MtMessage : "";
                                        mo.PinCodeID = -1;
                                        mo.MoStatus = MoStatus.WrongPinCode;
                                        // Wrong Pincode
                                    }
                                }
                                else
                                {
                                    responeMessage = (mtWrongPinCode != null) ? mtWrongPinCode.MtMessage : "";
                                    mo.PinCodeID = -1;
                                    mo.MoStatus = MoStatus.WrongPinCode;
                                }
                            }
                            else
                            {
                                responeMessage = (mtSubKeyIsDeactive != null) ? mtSubKeyIsDeactive.MtMessage : "";
                                mo.PinCodeID = (pinCode != null) ? pinCode.ID : -1;
                                mo.MoStatus = MoStatus.AccountIsDeactive;
                            }
                        }
                        else
                        {
                            responeMessage = defaultWrongSyntaxMessage;
                            // Wrong Syntax default
                            mo.PinCodeID = -1;
                            mo.MoStatus = MoStatus.WrongSyntax;
                        }
                    }
                    else
                    {
                        responeMessage = defaultWrongSyntaxMessage;
                        // Wrong Syntax default
                        mo.PinCodeID = -1;
                        mo.MoStatus = MoStatus.WrongSyntax;
                    }
                }
                else
                {
                    responeMessage = defaultWrongSyntaxMessage;
                    // Wrong Syntax default
                    mo.PinCodeID = -1;
                    mo.MoStatus = MoStatus.WrongSyntax;
                }

                #region Save Data
                mo.MoStatus = MoStatus.Success;
                if (moDao.Insert(mo))
                {
                    if (status)
                    {
                        pinCodeDao.UpdatePinCodeStatus((int)PinCodeStatus.Used, mo.PinCodeID.AsLong());
                    }
                    // Save MT
                    Mt mt = new Mt();
                    mt.MoID = mo.ID;
                    mt.PhoneNumber = phone;
                    mt.MtMessage = responeMessage;
                    mtDao.Insert(mt);
                }
                #endregion

                if (responeMessage == "")
                {
                    responeMessage = "He thong chua duoc cau hinh noi dung thong bao den khach hang. Ban vui long lien he nha cung cap san pham de duoc ho tro";
                }
                //responeMessage = message;
                return ReturnData(BuildMt(responeMessage, phone, service));
            }
            catch (Exception ex)
            {
                return ReturnData(BuildMt("Xay ra su co voi he thong. Ban vui long thu lai vao it giay sau", phone, service));
            }
            #region BK
            //Mo mo = new Mo
            //{
            //    MoMessage = message,
            //    PhoneNumber = phone,
            //    CreatedDate = DateTime.Now,
            //    PinCodeID = null,
            //    UpdatedDate = DateTime.Now,
            //    GUID = guid
            //};
            //moDao.Insert(mo);

            //try
            //{
            //    Config conf = con
            //    if (
            //        DateTime.Now.Subtract(Convert.ToDateTime(ConfigurationManager.AppSettings["ExpiredDate"])).TotalDays >=
            //        0)
            //    {
            //        mo.MoType = MoType.Expired;
            //        _moDao.Update(mo);
            //        mtMessage = config.MtExpired;
            //    }
            //    else
            //    {
            //        //xử lý trường hợp khách viết LIENA[4 số][Mã thẻ]
            //        if (messageArray.Count == 1 && messageArray[0].Length == 16)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0].Substring(0, 4),
            //                messageArray[0].Substring(4, 4),
            //                messageArray[0].Substring(8, 8)
            //            };
            //            messageArray = data;
            //        }// xử lý trường hợp khách nt LIENA[cách][4 số][Mã thẻ]
            //        else if (messageArray.Count == 2 && messageArray[1].Length == 12)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0],
            //                messageArray[1].Substring(0, 4),
            //                messageArray[1].Substring(4, 8)
            //            };
            //            messageArray = data;
            //        }//xử lý trường hợp khách nt LIENA[4 số][cách][mã thẻ]
            //        else if (messageArray.Count == 2 && messageArray[0].Length == 8)
            //        {
            //            List<string> data = new List<string>()
            //            {
            //                messageArray[0].Substring(0, 4),
            //                messageArray[0].Substring(4, 4),
            //                messageArray[1]
            //            };
            //            messageArray = data;
            //        }


            //        if (!main.ToLower().Equals(_keyword) || !sub.ToLower().Equals(_subkey) || !port.Equals(_port) ||
            //            (messageArray.Count < 3))
            //        {
            //            mo.MoType = MoType.WrongSyntax;
            //            _moDao.Update(mo);
            //            mtMessage = config.MtWrongSyntax;
            //        }
            //        else
            //        {
            //            string cardCode = messageArray[2];

            //            Card card = _cardDao.SearchCard(cardCode).SingleOrDefault();

            //            if (card != null && card.CardStatus == CardStatus.JustCreated)
            //            {
            //                mo.CardId = card.ID;
            //                mo.MoType = MoType.ValidCardNo;
            //                _moDao.Update(mo);

            //                card.CardStatus = CardStatus.Used;
            //                _cardDao.Update(card);

            //                int serviceTypeId = -1;
            //                if (card.Value == "10000")
            //                    serviceTypeId = 3817;
            //                else if (card.Value == "20000")
            //                    serviceTypeId = 3923;
            //                else if (card.Value == "30000")
            //                    serviceTypeId = 3924;
            //                else if (card.Value == "50000")
            //                    serviceTypeId = 3925;
            //                else if (card.Value == "100000")
            //                    serviceTypeId = 3926;
            //                else if (card.Value == "200000")
            //                    serviceTypeId = 3935;
            //                else
            //                    serviceTypeId = -1;

            //                try
            //                {
            //                    TopUp topup = new TopUp();
            //                    topup.PhoneNumber = mo.PhoneNumber;
            //                    topup.MoId = mo.ID;
            //                    topup.TopUpStatus = TopUpStatus.JustCreated;
            //                    topup.TopUpGuid = Guid.NewGuid().ToString();
            //                    topup.ServiceTypeId = serviceTypeId;
            //                    topup.SentTimes = 1;
            //                    topup.Message = "TopUp cho chuong trinh Lien A";
            //                    _topUpDao.Insert(topup);

            //                    string clientNo = ConfigurationManager.AppSettings["ClientNo"];
            //                    string password = ConfigurationManager.AppSettings["ClientPass"];

            //                    string sign = GetMD5(string.Format("{0}-{1}", clientNo, GetMD5(password)));
            //                    string result = _topupService.TopupMobile(clientNo, topup.PhoneNumber, topup.TopUpGuid,
            //                        serviceTypeId, sign, topup.Message);
            //                    //string result = "00";
            //                    topup.Remark = string.Format("FiboStatus:{0}", result);
            //                    if (result == "00")
            //                        topup.TopUpStatus = TopUpStatus.Pending;
            //                    else
            //                    {
            //                        topup.TopUpStatus = TopUpStatus.Fail;
            //                        topup.IsSentSms = IsSentSms.SentFail;
            //                    }

            //                    _topUpDao.Update(topup);
            //                }
            //                catch (Exception ex)
            //                {
            //                }

            //                mtMessage = config.MtSuccessContent.Replace("[card_value]", card.CardCode)
            //                    .Replace("[topup_value]", card.Value);
            //            }
            //            else if (card != null && card.CardStatus == CardStatus.Used)
            //            {
            //                mo.MoType = MoType.CardIsUsed;
            //                _moDao.Update(mo);

            //                mtMessage = config.MtCardUsed.Replace("[card_value]", card.CardCode);
            //            }
            //            else
            //            {
            //                mo.MoType = MoType.InvalidCarNo;
            //                _moDao.Update(mo);

            //                mtMessage = config.MtCardNotExits.Replace("[card_value]", cardCode);
            //                ;
            //            }
            //        }
            //    }

            //    var mt = new Mt
            //    {
            //        MoId = mo.ID,
            //        ID = 0,
            //        CreatedDate = DateTime.Now,
            //        UpdatedDate = DateTime.Now,
            //        PhoneNumber = phone,
            //        Message = mtMessage
            //    };
            //    _mtDao.Insert(mt);

            //    return ReturnData(BuildMt(mt.Message, phone, service));
            //}
            //catch (Exception ex)
            //{
            //    //lưu Mo trạng thái sai cú pháp
            //    mo.MoType = MoType.WrongSyntax;
            //    _moDao.Insert(mo);

            //    // trả về tin sai cú pháp
            //    var mt = new Mt
            //    {
            //        MoId = mo.ID,
            //        ID = 0,
            //        CreatedDate = DateTime.Now,
            //        UpdatedDate = DateTime.Now,
            //        PhoneNumber = phone,
            //        Message = config.MtWrongSyntax,

            //    };
            //    _mtDao.Insert(mt);
            //    return ReturnData(BuildMt(mt.Message, phone, service));
            //}
            #endregion
        }


        private string BuildMt(string mt, string phone, string service)
        {
            if (mt.Length > 160)
            {
                string mess = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ClientResponse>";
                string childmess = string.Empty;
                string[] array = mt.Split(' ');
                for (int i = 0; i < array.Length; i++)
                {
                    if ((childmess.Length + array[i].Length + 1) > 160)
                    {
                        mess += "<Message><PhoneNumber>" + phone + "</PhoneNumber>" +
                                "<Message>" + childmess.Trim() + "</Message>" +
                                "<SMSID>" + Guid.NewGuid() + "</SMSID>" +
                                "<ServiceNo>" + service + "</ServiceNo>" +
                                "<ContentType>0</ContentType>" +
                                "</Message>";
                        childmess = string.Empty;
                    }
                    else
                    {
                        childmess += array[i] + " ";
                    }
                }
                mess += "</ClientResponse>";
                return mess;
            }
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                   "<ClientResponse><Message>" +
                   "<PhoneNumber>" + phone + "</PhoneNumber>" +
                   "<Message>" + mt + "</Message>" +
                   "<SMSID>" + Guid.NewGuid().ToString() + "</SMSID>" +
                   "<ServiceNo>" + service + "</ServiceNo>" +
                   "<ContentType>0</ContentType>" +
                   "</Message></ClientResponse>";
        }

    }
}
