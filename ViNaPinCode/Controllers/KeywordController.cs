﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;

namespace VinaVN.Controllers
{
    public class KeywordController : BaseController
    {
        //
        // GET: /Keyword/
        private readonly SqlKeywordDao _dao = new SqlKeywordDao();
        private readonly SqlAccountDao _accountDao = new SqlAccountDao();

        public ActionResult Index(KeywordViewModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;
            if (model.IsPopup)
                model.Page.PageSize = 0;

            model.DataList = _dao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.AccountManagerID, model.Keyword ?? "", (int)model.KeywordStatus);

            var firstOrDefault = model.DataList.FirstOrDefault();
            if (firstOrDefault != null)
                model.Page.TotalRecords = (model.DataList.Any())
                    ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                    : 0;
            else
                model.Page.TotalRecords = 0;
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null && Session["ErrorMessage"] != "") ? Session["ErrorMessage"].ToString() : "";
            Session["ErrorMessage"] = "";
            return View(model);
        }

        public ActionResult AutoLogin(long suid = -1)
        {
            Account newLogin = _accountDao.CheckAutoLogin(suid, SAccount.ID);
            if (newLogin != null)
            {
                Session["ErrorMessage"] = "";
                SAccount = newLogin;
                return Redirect("/Home");
            }
            Session["ErrorMessage"] = "Đăng nhập tự động không thành công";
            return Redirect("/Account/Login");
        }

        public ActionResult CreateNewKeyword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewKeyword(KeywordViewModel model)
        {
            if (_dao.CheckExists(model.Keyword, model.LoginName) > 0)
            {
                ViewBag.ErrorMessage = "Subkey hoặc tên đăng nhập đã tồn tại. Vui lòng thay đổi";
                return View(model);
            }
            //Account acc = new Account();
            //acc.LoginName = model.LoginName;
            //acc.PassWord = GFunction.GetMD5(model.Password);
            //acc.PhoneNumber = model.PhoneNumber;
            //acc.EmailAddress = model.EmailAddress;
            //acc.ParentId = SAccount.ID;
            //acc.FullName = model.LoginName;
            //if (_accountDao.Insert(acc))
            //{
            Keyword key = new Keyword();
            key.AccountManagerID = -1;
            key.KeywordValue = model.Keyword;
            key.KeywordStatus = KeywordStatus.Active;
            key.TotalProductType = model.TotalProductType;
            if (_dao.Insert(key))
            {
                ViewBag.SuccessMessage = "Đã thêm keyword thành công";
            }
            else
            {
                ViewBag.ErrorMessage = "Thêm keyword thất bại";
            }
            //}
            return View(model);
        }

        public ActionResult ChangeStatus(long suid, int status)
        {
            if (_dao.ChangeStatus(suid, status) > 0)
            {
                Session["SuccessMessage"] = "Cập nhật thành công";
            }
            else
            {
                Session["ErrorMessage"] = "Cập nhật thất bại";
            }
            return Redirect("/Home/ManagerPanel");
        }

    }
}
