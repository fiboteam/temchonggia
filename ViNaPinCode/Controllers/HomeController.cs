﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using Utility;

namespace VinaVN.Controllers
{
    public class HomeController : BaseController
    {
        private readonly SqlSubKeyDao _subKeyDao = new SqlSubKeyDao();
        private readonly SqlMoDao _moDao = new SqlMoDao();
        private readonly SqlPinCodeDao _pingCodeDao = new SqlPinCodeDao();
        private readonly SqlKeywordDao _keywordDao = new SqlKeywordDao();
        public ActionResult Index()
        {
            if (SAccount.ParentId == 0)
            {
                return Redirect("/Home/ManagerPanel");
            }
            ViewBag.MoCount = _moDao.CountMoByAccountID(SAccount.ID);
            ViewBag.PinCodeCount = _pingCodeDao.CountPinCodeByAccountID(SAccount.ID);
            ViewBag.PinCodeIsUsedCount = _pingCodeDao.CountPinCodeIsUsedByAccountID(SAccount.ID);
            return View();
        }

        public ActionResult ManagerPanel()
        {
            if(SAccount.ParentId > 0)
            {
                return Redirect("/Home");
            }

            List<Keyword> listKeyword = new List<Keyword>();
            listKeyword = _keywordDao.GetWithFilter(100000, 1, SAccount.ID, "", (int)SubKeyStatus.Active);
            ViewBag.ListKeyword = listKeyword;
            ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            ViewBag.IsGenerate = (Session["ListCodeGenerate"] != null) ? true : false;
            Session["SuccessMessage"] = "";
            Session["ErrorMessage"] = "";
            return View();
        }

        [HttpPost]
        public ActionResult SendRequest(HelpFormModel model)
        {
            try
            {
                string body = "Có thông tin feedback từ hệ thống chống hàng giả<br />";
                body += "Thông tin như sau : <br />";
                body += "Người feedback : " + SAccount.LoginName + "(" + SAccount.FullName + ")<br />";
                body += "Email : " + SAccount.EmailAddress + "<br />";
                body += "Điện thoại : " + SAccount.PhoneNumber + "<br />";
                body += "Nội dung : " + model.Message;
                System.Net.Mail.MailMessage objeto_mail = new System.Net.Mail.MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 26;
                client.Host = "103.246.220.25";
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential("OTP-YBVTHUAXHADF", "YBVTHA");
                objeto_mail.IsBodyHtml = true;
                objeto_mail.From = new MailAddress("tri.ntm@fibo.vn", "MT");
                objeto_mail.To.Add(new MailAddress("tri.ntm@fibo.vn"));
                objeto_mail.To.Add(new MailAddress("tu.pvm@fibo.vn"));
                objeto_mail.Subject = "[Feedback] SMS chống hàng giả";
                objeto_mail.Body = body;
                client.Send(objeto_mail);
                Session["SuccessMessage"] = "Yêu cầu của quý khách đã được tiếp nhận";
            }
            catch (Exception ex)
            {
                Session["ErrorMessage"] = "Gửi yêu cầu thất bại";
            }
            return Redirect("/Home");
        }
    }
}
