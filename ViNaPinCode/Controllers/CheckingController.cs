﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;

namespace ViNaPinCode.Controllers
{
   
    public class CheckingController : Controller
    {
        private readonly SqlPinCodeDao _pinCodeDao = new SqlPinCodeDao();
       public ActionResult Index()
        {
            PinCodeViewModel model = new PinCodeViewModel();
            model.DataList = new List<PinCodeObj>();
            return View(model);
        }

       [HttpPost]
       public ActionResult Index(PinCodeViewModel model)
       {
           if (Session["Captcha"] == null || Session["Captcha"].ToString() != model.Captcha)
           {
               ModelState.AddModelError("Captcha", "Mã xác thực captcha không chính xác");
               return View(model);
           }
           model.DataList = _pinCodeDao.Checking(model.PinCode, model.Serial);
           if(model.DataList.Count() > 0)
           {
               long pincodeId = model.DataList.FirstOrDefault().ID;
               _pinCodeDao.UpdatePinCodeStatus((int)PinCodeStatus.Used, pincodeId);
           }
           return View(model);
       }
    }
}
