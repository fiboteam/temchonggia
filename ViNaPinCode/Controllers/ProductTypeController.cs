﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using Vina.DAO;
using Vina.DTO;
using ViNaVN.Models;

namespace VinaVN.Controllers
{
    public class ProductTypeController : BaseController
    {
        //
        // GET: /ProductType/
        public SqlProductTypeDao _dao = new SqlProductTypeDao();

        public ActionResult Index(ProductTypeViewModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;

            model.DataList = _dao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage,SAccount.ID,model.ProductTypeName ?? "", model.ProductTypeKey ?? "",(int)model.ProductTypeStatus);

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;

            }
            ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            Session["SuccessMessage"] = "";
            Session["ErrorMessage"] = "";
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(ProductTypeViewModel model)
        {
            ProductType type = new ProductType();
            type.ProductTypeName = model.ProductTypeName;
            type.ProductTypeKey = model.ProductTypeKey;
            type.ProductTypeStatus = ProductTypeStatus.Active;
            type.ManagerID = SAccount.ID;
            type.CreatedDate = DateTime.Now;
            type.UpdatedDate = DateTime.Now;
            if(_dao.Insert(type))
            {
                SuccessMessage = "Đã thêm thành công " + model.ProductTypeName + " vào danh sách. Cú pháp nhắn tin áp dụng là : " + model.ProductTypeKey;
            }
            else
            {
                ErrorMessage = "Thao tác thất bại";
            }
            return Redirect("/ProductType");
        }

    }
}
