﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using System.Threading.Tasks;

namespace VinaVN.Controllers
{
    public class PinCodeController : BaseController
    {
        private readonly SqlPinCodeDao _dao = new SqlPinCodeDao();
        private readonly SqlPinCodeBlockDao _pinCodeBlockDao = new SqlPinCodeBlockDao();
        private readonly SqlAccountDao _accountDao = new SqlAccountDao();
        private readonly SqlKeywordDao _keywordDao = new SqlKeywordDao();
        private readonly SqlPinCodeTempDao _pincodeTempDao = new SqlPinCodeTempDao();
        private readonly SqlProductTypeDao _productTypeDao = new SqlProductTypeDao();
        private readonly SqlSubKeyDao _subKeyDao = new SqlSubKeyDao();
        public ActionResult Index(PinCodeViewModel model)
        {
            if (Request.Form["submitButton"] == "Export")
            {
                ProcessExport(model);
                return Redirect("/PinCode");
            }
            else
            {
                if (model.CurrentPage != 0)
                    model.Page.CurrentPage = model.CurrentPage;
                else
                    model.Page.CurrentPage = 1;
                if (model.IsPopup)
                    model.Page.PageSize = 0;

                model.DataList = _dao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.PinCode ?? "", model.Serial ?? "", (int)model.PinCodeStatus, SAccount.ID, model.FromDate, model.ToDate, model.PinCodeBlockID);

                if (model.DataList != null && model.DataList.Count() > 0)
                {
                    var firstOrDefault = model.DataList.FirstOrDefault();
                    if (firstOrDefault != null)
                        model.Page.TotalRecords = (model.DataList.Any())
                            ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                            : 0;
                    else
                        model.Page.TotalRecords = 0;

                }
                List<PinCodeBlock> blockList = _pinCodeBlockDao.GetList(SAccount.ID);

                PinCodeBlock all = new PinCodeBlock();
                all.ID = 0;
                all.PinCodeBlockName = "Tất cả";
                blockList.Insert(0, all);

                ViewBag.BlockList = new SelectList(blockList, "ID", "PinCodeBlockName");
                ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
                ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
                ViewBag.FailListCount = (Session["DeplicatedMessage"] != null) ? Session["DeplicatedMessage"] : "";
                ViewBag.WrongSyntaxListCount = (Session["WrongSyntaxMessage"] != null) ? Session["WrongSyntaxMessage"] : "";
                ViewBag.BlockID = (Session["BlockID"] != null) ? Convert.ToInt64(Session["BlockID"]) : -1;
                Session["SuccessMessage"] = "";
                Session["ErrorMessage"] = "";
                Session["WrongSyntaxMessage"] = "";
                Session["DeplicatedMessage"] = "";
                return View(model);
            }
        }

        public ActionResult AjaxShowFormImport()
        {
            List<PinCodeBlock> blockList = _pinCodeBlockDao.GetList(SAccount.ID);
            ViewBag.BlockList = new SelectList(blockList, "ID", "PinCodeBlockName");

            List<ProductType> productTypeList = _productTypeDao.GetProductTypeByAccountID(SAccount.ID);
            ViewBag.ProductTypeList = new SelectList(productTypeList, "ID", "ProductTypeName");

            return View();
        }

        public ActionResult CreateBlock()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateBlock(PinCodeBlockViewModel model)
        {
            try
            {
                PinCodeBlock block = new PinCodeBlock();
                block.PinCodeBlockName = model.PinCodeBlockName;
                block.PinCodeBlockStatus = Utility.PinCodeBlockStatus.Active;
                block.AccountManagerID = SAccount.ID;
                if (_pinCodeBlockDao.Insert(block))
                {
                    Session["SuccessMessage"] = "Đã tạo block thành công";
                }
                else
                {
                    Session["ErrorMessage"] = "Tạo block thất bại";
                }
            }
            catch (Exception ex) { }
            return Redirect("/PinCode");
        }

        public ActionResult AddPinCode()
        {
            List<PinCodeBlock> blockList = _pinCodeBlockDao.GetList(SAccount.ID);
            ViewBag.BlockList = new SelectList(blockList, "ID", "PinCodeBlockName");

            List<ProductType> productTypeList = _productTypeDao.GetProductTypeByAccountID(SAccount.ID);
            ViewBag.ProductTypeList = new SelectList(productTypeList, "ID", "ProductTypeName");

            return View();
        }

        [HttpPost]
        public ActionResult AddPinCode(PinCodeViewModel model)
        {
            List<PinCodeBlock> blockList = _pinCodeBlockDao.GetList(SAccount.ID);
            ViewBag.BlockList = new SelectList(blockList, "ID", "PinCodeBlockName");

            if (_dao.CheckPinCodeExists(model.PinCode, SAccount.ID) > 0)
            {
                Session["ErrorMessage"] = "PinCode đã tồn tại trong hệ thống";
                return Redirect("/Pincode");
            }
            PinCodeObj pc = new PinCodeObj();
            pc.PinCode = model.PinCode;
            pc.PinCodeBlockID = model.PinCodeBlockID;
            pc.Serial = model.Serial;
            pc.ProductTypeID = model.ProductTypeID;
            pc.Serial2 = "";
            pc.AccountManagerID = SAccount.ID;
            pc.PinCodeStatus = Utility.PinCodeStatus.JustCreated;
            if (_dao.Insert(pc))
            {
                Session["SuccessMessage"] = "Đã thêm thành công pincode";
            }
            else
            {
                Session["ErrorMessage"] = "Thêm pincode thất bại";
            }
            return Redirect("/Pincode");
        }

        [HttpPost]
        public ActionResult Import(PinCodeImportViewModel model, HttpPostedFileBase fileImport = null)
        {
            string path = "/Userfile/FileImport";
            string returnPath = path + "/" + UploadFile(fileImport, path, null);
            ExecImport(model, returnPath);
            return Redirect("/PinCode");
        }

        #region Generate Region

        

        #endregion

        public ActionResult AjaxLoadBlockListForAccount(long subKeyId)
        {
            SubKey sk = _subKeyDao.GetSingle(subKeyId);
            if (sk != null)
            {
                List<PinCodeBlock> pinCodeBlockList = _pinCodeBlockDao.GetList(sk.AccountManagerID);
                PinCodeBlock createNew = new PinCodeBlock();
                createNew.ID = 0;
                createNew.PinCodeBlockName = "Tạo mới block";
                pinCodeBlockList.Insert(0, createNew);
                ViewBag.PinCodeBlockList = new SelectList(pinCodeBlockList, "ID", "PinCodeBlockName");
            }
            return View();
        }

        public ActionResult AjaxLoadSubKeyForAccount(long keywordId)
        {
            List<SubKey> subKeyList = _subKeyDao.GetWithFilter(1000, 1, -1, keywordId, "", 0);
            if (subKeyList.Count() <= 0)
            {
                return Content("<div style = 'width:100%;color:red;text-align:center;'>Vui lòng tạo subkey trước</div>");
            }
            ViewBag.SubKeyList = new SelectList(subKeyList, "ID", "SubKeyValue");
            return View();
        }

        public void ExecImport(PinCodeImportViewModel model, string filePath)
        {
            try
            {
                List<PinCodeObj> pinCodeFails = new List<PinCodeObj>();
                List<PinCodeObj> pinCodeSuccess = new List<PinCodeObj>();
                List<PinCodeObj> pinCodeWrongSyntax = new List<PinCodeObj>();

              
                //string[] allowExtention = new[] { "xls", "xlsx" };
                
                //allowExtention = allowExtention.Where(item => item != null).Select(item => item.ToLower()).ToArray();
                //var fileExt = System.IO.Path.GetExtension(filePath).Substring(1);
                /////Userfile/FileImport/2_temp code(4).xls
                /////
                //var path = Path.Combine(Server.MapPath(filePath));
                //ISheet sheet;
                //if (fileExt.ToLower()=="xls")
                //{
                //    HSSFWorkbook hssfworkbook;
                //    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                //    {
                //        hssfworkbook = new HSSFWorkbook(file);
                //    }
                //    sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                //}
                //else
                //{
                //    XSSFWorkbook hssfworkbook = new XSSFWorkbook();
                //    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                //    {
                //        hssfworkbook = new XSSFWorkbook(file);
                //    }
                //    sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                //}

                var path = Path.Combine(Server.MapPath(filePath));
                HSSFWorkbook hssfworkbook;
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    hssfworkbook = new HSSFWorkbook(file);
                }
                ISheet sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

                while (rows.MoveNext())
                {
                    IRow row = (HSSFRow)rows.Current;
                    ICell cellCardCode = row.GetCell((int)model.ExcelColunmPinCode);
                    ICell cellSerialCode = row.GetCell((int)model.ExcelColunmSerialCode);
                    //ICell cellSerialCode2 = row.GetCell((int)model.ExcelColunmSerialCode2);

                    PinCodeObj pincode = new PinCodeObj();

                    try
                    {
                        pincode.PinCode = (cellCardCode != null) ? cellCardCode.StringCellValue : "N/A";
                    }
                    catch (Exception ex)
                    {
                        pincode.PinCode = (cellCardCode != null) ? cellCardCode.NumericCellValue.ToString() : "N/A";
                    }
                    try
                    {
                        pincode.Serial = (cellSerialCode != null) ? cellSerialCode.NumericCellValue.ToString() : "N/A";
                    }
                    catch (Exception ex)
                    {
                        pincode.Serial = (cellSerialCode != null) ? cellSerialCode.StringCellValue.ToString() : "N/A";
                    }
                    finally
                    {
                        pincode.Serial = "";
                    }
                    //try
                    //{
                    //    pincode.Serial2 = (cellSerialCode2 != null) ? cellSerialCode2.StringCellValue.ToString() : "N/A";
                    //}
                    //catch(Exception ex)
                    //{
                    //    pincode.Serial2 = (cellSerialCode2 != null) ? cellSerialCode2.NumericCellValue.ToString() : "N/A";
                    //}


                    pincode.AccountManagerID = SAccount.ID;
                    pincode.PinCodeStatus = Utility.PinCodeStatus.JustCreated;
                    pincode.PinCodeBlockID = model.PinCodeBlockID;
                    pincode.ProductTypeID = model.ProductTypeID;
                    if (_dao.CheckPinCodeExists(pincode.PinCode, SAccount.ID) == 0)
                    {
                        if (pincode.PinCode.Length > 4)
                        {
                            if (_dao.Insert(pincode))
                            {
                                pinCodeSuccess.Add(pincode);
                            }
                            else
                            {
                                if (pinCodeFails.Where(m => m.PinCode == pincode.PinCode).Count() <= 0)
                                {
                                    pinCodeFails.Add(pincode);
                                }
                            }
                        }
                        else
                        {
                            if (pinCodeFails.Where(m => m.PinCode == pincode.PinCode).Count() <= 0)
                            {
                                pinCodeFails.Add(pincode);
                            }
                        }
                    }
                    else
                    {
                        if (pinCodeFails.Where(m => m.PinCode == pincode.PinCode).Count() <= 0)
                        {
                            pinCodeFails.Add(pincode);
                        }
                    }
                }
                if (pinCodeFails.Count() > 0)
                {
                    Session["FailList"] = pinCodeFails;
                    Session["DeplicatedMessage"] = "Tổng số PinCode bị trùng : " + pinCodeFails.Count();
                }
                if (pinCodeWrongSyntax.Count() > 0)
                {
                    Session["WrongSyntaxList"] = pinCodeWrongSyntax;
                    Session["WrongSyntaxMessage"] = "Tổng số PinCode sai cú pháp : " + pinCodeWrongSyntax.Count();
                }
                SuccessMessage = "Tổng số PinCode được nhập thành công : " + pinCodeSuccess.Count();
                Session["BlockID"] = model.PinCodeBlockID;
                Session["SuccessList"] = pinCodeSuccess;
                return;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Có lỗi phát sinh trong quá trình upload hoặc chọn data";
            }
        }

        public ActionResult ExportListAfterImport(PinCodeListType type, long blockId)
        {
            string fileName = "list.xls";
            if (type == PinCodeListType.Success)
            {
                List<PinCodeObj> successList = (List<PinCodeObj>)Session["SuccessList"]; // _dao.GetWithFilter(0, 1, "", "", "", 0, SAccount.ID, DateTime.Now.AddDays(-365), DateTime.Now.AddDays(365), blockId);
                fileName = "successList_" + blockId.ToString() + ".xls";
                ExportToExcel(successList, fileName);
            }
            if (type == PinCodeListType.Duplicated)
            {
                List<PinCodeObj> duplicatedList = (List<PinCodeObj>)Session["FailList"];
                fileName = "duplicatedList_" + blockId.ToString() + ".xls";
                ExportToExcel(duplicatedList, fileName);
            }
            if (type == PinCodeListType.WrongSyntax)
            {
                List<PinCodeObj> wrongSyntaxList = (List<PinCodeObj>)Session["WrongSyntaxList"];
                fileName = "wrongSyntaxList_" + blockId.ToString() + ".xls";
                ExportToExcel(wrongSyntaxList, fileName);
            }
            return Redirect("/PinCode");
        }


        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<PinCodeObj> pinCodeList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách mã số");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Pincode");
            //rowheader.CreateCell(1).SetCellValue("Số Serial 1");
            //rowheader.CreateCell(2).SetCellValue("Số Serial 2");
            rowheader.CreateCell(1).SetCellValue("Trạng thái");
            rowheader.CreateCell(2).SetCellValue("Ngày nhập");
            rowheader.CreateCell(3).SetCellValue("Ngày cập nhật");

            int i = 2;
            if (pinCodeList == null)
                return;
            foreach (PinCodeObj pinCode in pinCodeList)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(j).SetCellValue(pinCode.PinCode);
                j++;
                //row.CreateCell(j).SetCellValue(pinCode.Serial.ToString());
                //j++;
                //row.CreateCell(j).SetCellValue(pinCode.Serial2.ToString());
                //j++;
                row.CreateCell(j).SetCellValue(pinCode.PinCodeStatus.ToString());
                j++;
                row.CreateCell(j).SetCellValue(pinCode.CreatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                row.CreateCell(j).SetCellValue(pinCode.UpdatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                i++;
            }
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        public ActionResult ProcessExport(PinCodeViewModel model)
        {
            List<PinCodeObj> cardList = _dao.GetWithFilter(0, 1, model.PinCode ?? "", model.Serial ?? "", (int)model.PinCodeStatus, SAccount.ID, model.FromDate, model.ToDate, model.PinCodeBlockID);
            string fileName = "pincodelist_" + Guid.NewGuid().ToString() + ".xls";
            ExportToExcel(cardList, fileName);
            return Redirect("/PinCode");
        }

        public void ExportToExcel(List<PinCodeObj> cardList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, cardList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }

        private ICell GetCell(IRow row, int column)
        {
            ICell cell = row.GetCell(column);
            if (cell == null)
                return row.CreateCell(column);
            return cell;
        }

        public ActionResult Delete(long pcid)
        {

            if (_dao.DeletePC(pcid) > 0)
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }

            return Redirect("/PinCode");
        }

    }
}
