﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VinaVN.Controllers
{
    public class ConfigController : BaseController
    {
        //
        // GET: /Config/
        private readonly SqlConfigDao _dao = new SqlConfigDao();

        public ActionResult ChangeConfig()
        {
            ConfigModel model = new ConfigModel();
            Config conf = _dao.GetConfigByAccountID(SAccount.ID);
            try
            {
                model.StartDate = conf.StartDate;
                model.EndDate = conf.EndDate;
            }
            catch(Exception ex)
            {
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now.AddDays(30);
            }
            ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            Session["SuccessMessage"] = "";
            Session["ErrorMessage"] = "";
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveConfigChange(ConfigModel model)
        {
            Config conf = new Config();
            conf.StartDate = model.StartDate;
            conf.EndDate = model.EndDate;
            conf.AccountManagerID = SAccount.ID;
            conf.SubKeyID = -1;
            if(_dao.Insert(conf))
            {
                Session["SuccessMessage"] = "Thay đổi đã được áp dụng";
            }
            else
            {
                Session["ErrorMessage"] = "Thay đổi thất bại";
            }
            return Redirect("/Config/ChangeConfig");
        }

    }
}
