﻿using System;
using System.Linq;
using System.Web.Mvc;
using Vina.DAO;
using VinaVN.Models;

namespace VinaVN.Controllers
{
    public class MtController : BaseController
    {
       private readonly SqlMtDao _mtDao = new SqlMtDao();

        public ActionResult Index(MtViewModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;
            if (model.IsPopup)
                model.Page.PageSize = 0;

            model.DataList = _mtDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.MoId,model.FromDate,model.ToDate, model.Phone??"","",SAccount.ID);

            var firstOrDefault = model.DataList.FirstOrDefault();
            if (firstOrDefault != null)
                model.Page.TotalRecords = (model.DataList.Any())
                    ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                    : 0;
            else
                model.Page.TotalRecords = 0;

            return View(model);
        }

    }
}
