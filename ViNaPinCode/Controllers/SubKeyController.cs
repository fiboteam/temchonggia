﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace VinaVN.Controllers
{
    public class SubKeyController : BaseController
    {
        //
        // GET: /SubKey/

        private readonly SqlSubKeyDao _dao = new SqlSubKeyDao();
        private readonly SqlAccountDao _accountDao = new SqlAccountDao();
        private readonly SqlKeywordDao _keywordDao = new SqlKeywordDao();

        public ActionResult Index(SubKeyViewModel model)
        {
            #region SubKey
            //if (model.CurrentPage != 0)
            //    model.Page.CurrentPage = model.CurrentPage;
            //else
            //    model.Page.CurrentPage = 1;
            //if (model.IsPopup)
            //    model.Page.PageSize = 0;

            //model.DataList = _dao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.AccountManagerID,model.KeywordID, model.SubKeyValue ?? "", (int)model.SubKeyStatus);

            //var firstOrDefault = model.DataList.FirstOrDefault();
            //if (firstOrDefault != null)
            //    model.Page.TotalRecords = (model.DataList.Any())
            //        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
            //        : 0;
            //else
            //    model.Page.TotalRecords = 0;
            //ViewBag.ErrorMessage = (Session["ErrorMessage"] != null && Session["ErrorMessage"] != "") ? Session["ErrorMessage"].ToString() : "";
            //Session["ErrorMessage"] = "";
            //return View(model);
            #endregion

            if (SAccount.ParentId > 0)
            {
                return Redirect("/Home");
            }

            List<SubKey> listSubkey = new List<SubKey>();
            listSubkey = _dao.GetWithFilter(1000, 1, model.AccountManagerID, model.KeywordID, model.SubKeyValue ?? "", (int)model.SubKeyStatus);
            ViewBag.ListSubKey = listSubkey;
            ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            //ViewBag.IsGenerate = (Session["ListCodeGenerate"] != null) ? true : false;
            Session["SuccessMessage"] = "";
            Session["ErrorMessage"] = "";
            return View();
        }

        public ActionResult AutoLogin(long suid = -1)
        {
            Account newLogin = _accountDao.CheckAutoLogin(suid, SAccount.ID);
            if (newLogin != null)
            {
                Session["ErrorMessage"] = "";
                SAccount = newLogin;
                return Redirect("/Home");
            }
            Session["ErrorMessage"] = "Đăng nhập tự động không thành công";
            return Redirect("/Account/Login");
        }

        public ActionResult CreateNewSubKey()
        {
            List<Keyword> allKeyword = _keywordDao.GetAllKeyword();
            Keyword kw = new Keyword();
            kw.ID = 0;
            kw.KeywordValue = " - Chọn Keyword - ";
            allKeyword.Insert(0, kw);
            ViewBag.KeywordList = new SelectList(allKeyword, "ID", "KeywordValue");
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewSubKey(SubKeyViewModel model)
        {
            if(_dao.CheckExists(model.SubKeyValue,model.LoginName) > 0)
            {
                ViewBag.ErrorMessage = "Subkey hoặc tên đăng nhập đã tồn tại. Vui lòng thay đổi";
                return View(model);
            }
            Account acc = new Account();
            acc.LoginName = model.LoginName;
            acc.PassWord = GFunction.GetMD5(model.Password);
            acc.PhoneNumber = model.PhoneNumber;
            acc.EmailAddress = model.EmailAddress;
            acc.ParentId = SAccount.ID;
            acc.FullName = model.LoginName;
            if(_accountDao.Insert(acc))
            {
                SubKey sub = new SubKey();
                sub.AccountManagerID = acc.ID;
                sub.SubKeyValue = model.SubKeyValue;
                sub.KeywordID = model.KeywordID;
                sub.SubKeyStatus = SubKeyStatus.Active;
                if(_dao.Insert(sub))
                {
                    ViewBag.SuccessMessage = "Đã tạo subkey thành công";
                }
                else
                {
                    ViewBag.ErrorMessage = "Tạo Subkey thất bại";
                }
            }
            List<Keyword> allKeyword = _keywordDao.GetAllKeyword();
            Keyword kw = new Keyword();
            kw.ID = 0;
            kw.KeywordValue = " - Chọn Keyword - ";
            allKeyword.Insert(0, kw);
            ViewBag.KeywordList = new SelectList(allKeyword, "ID", "KeywordValue");
            return View(model);
        }

        public ActionResult ChangeStatus(long suid,int status)
        {
            SubKey subKey = _dao.GetSingle(suid);
            if(_dao.ChangeStatus(suid,status) > 0)
            {
                Session["SuccessMessage"] = "Cập nhật thành công";
            }
            else
            {
                Session["ErrorMessage"] = "Cập nhật thất bại";
            }
            return Redirect("/SubKey/?KeywordID=" + subKey.KeywordID);
        }
    }
}
