﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using VinaVN.Models;
using Vina.DAO;
using Vina.DTO;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Utility;

namespace VinaVN.Controllers
{
    public class MoController : BaseController
    {
        private readonly SqlMoDao _moDao = new SqlMoDao();

        public ActionResult CountMo()
        {
            _moDao.Count();
            return View(_moDao.Count());
        }

        public ActionResult ProcessExport(MoViewModel model)
        {
            //if (!string.IsNullOrEmpty(model.FromDateTime) && !string.IsNullOrEmpty(model.FromDateTime))
            //{
            //    model.FromDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.FromDateTime)));
            //    model.ToDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.ToDateTime)));
            //}
            //else
            //{
            //    model.FromDate = model.ToDate = 0;
            //    model.FromDateTime =  string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            //    model.ToDateTime = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            //}
            model.Page.PageSize = 0;
            model.DataList = _moDao.GetWithFilter(100000, 1, model.PinCode ?? "",
                model.FromDate,
                model.ToDate, model.PhoneNumber ?? "", model.MoMessage ?? "", (int)model.MoStatus,-1,SAccount.ID);
            string fileName = "sms_recever_" + model.MoStatus.ToString() + "_" + Guid.NewGuid().ToString() + ".xls";
            ExportToExcel(model.DataList, fileName);
            return Redirect("/Mo");
        
        }

      
        public void ExportToExcel(List<Mo> moList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, moList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }
        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<Mo> moList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            IRow head = sheet1.CreateRow(0);
            head.CreateCell(0).SetCellValue("PhoneNumber");
            head.CreateCell(1).SetCellValue("PinCode");
            head.CreateCell(2).SetCellValue("Nội dung");
            head.CreateCell(3).SetCellValue("Trạng thái");
            head.CreateCell(4).SetCellValue("Ngày nhắn");
            
            int i = 1;
            if (moList == null)
                return;
            foreach (Mo mo in moList)
            {
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(0).SetCellValue(mo.PhoneNumber);
                row.CreateCell(1).SetCellValue(mo.ExtentionProperty["PinCode"].ToString());
                row.CreateCell(2).SetCellValue(mo.MoMessage);
                row.CreateCell(3).SetCellValue(mo.MoStatus.ToString());
                row.CreateCell(4).SetCellValue(mo.CreatedDate.ToString());
                i++;
            }
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        private ICell GetCell(IRow row, int column)
        {
            ICell cell = row.GetCell(column);
            if (cell == null)
                return row.CreateCell(column);
            return cell;
        }

        public ActionResult Index(MoViewModel model)
        {
            try
            {
                //if (!string.IsNullOrEmpty(model.FromDateTime) && !string.IsNullOrEmpty(model.FromDateTime))
                //{
                //    model.FromDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.FromDateTime)));
                //    model.ToDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.ToDateTime)));
                //}
                //else
                //{
                //    model.FromDate = model.ToDate = 0;
                //    model.FromDateTime =  string.Format("{0:dd-MM-yyyy}", DateTime.Now);
                //    model.ToDateTime = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
                //}
                if (Request.Form["submitButton"] == "Export")
                {
                    ProcessExport(model);
                    return Redirect("/Mo");
                }
                else
                {
                    if (model.CurrentPage != 0)
                            model.Page.CurrentPage = model.CurrentPage;
                        else
                            model.Page.CurrentPage = 1;
                        if (model.IsPopup)
                            model.Page.PageSize = 0;

                        model.DataList = _moDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.PinCode ?? "",
                            model.FromDate,
                            model.ToDate, model.PhoneNumber ?? "", model.MoMessage ?? "", (int)model.MoStatus, model.MoId, SAccount.ID);

                        var firstOrDefault = model.DataList.FirstOrDefault();
                        if (firstOrDefault != null)
                            model.Page.TotalRecords = (model.DataList.Any())
                                ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                                : 0;
                        else
                            model.Page.TotalRecords = 0;
                        return View(model);
                    }
                }
            catch (Exception)
            {
                return View(model);
            }
        }

        public ActionResult ViewAllMoErrorSyntax(MoViewModel model)
        {
            try
            {
                //if (!string.IsNullOrEmpty(model.FromDateTime) && !string.IsNullOrEmpty(model.FromDateTime))
                //{
                //    model.FromDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.FromDateTime)));
                //    model.ToDate = int.Parse(string.Format("{0:yyyyMMdd}", GFunction.ConvertToMMddyyyy(model.ToDateTime)));
                //}
                //else
                //{
                //    model.FromDate = model.ToDate = 0;
                //    model.FromDateTime = "";// string.Format("{0:dd-MM-yyyy}", DateTime.Now);
                //    model.ToDateTime = "";//string.Format("{0:dd-MM-yyyy}", DateTime.Now);
                //}
                if (model.CurrentPage != 0)
                    model.Page.CurrentPage = model.CurrentPage;
                else
                    model.Page.CurrentPage = 1;
                if (model.IsPopup)
                    model.Page.PageSize = 0;

                model.DataList = _moDao.ViewAllMoErrorSyntax(model.Page.PageSize, model.Page.CurrentPage, model.PinCode ?? "",
                    model.FromDate,
                    model.ToDate, model.PhoneNumber ?? "", model.MoMessage ?? "", (int)model.MoStatus, model.MoId, -1);

                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;
                return View(model);
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<PinCodeObj> pinCodeList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách mã số");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Pincode");
            rowheader.CreateCell(1).SetCellValue("Số Serial 1");
            //rowheader.CreateCell(2).SetCellValue("Số Serial 2");
            rowheader.CreateCell(2).SetCellValue("Trạng thái");
            rowheader.CreateCell(3).SetCellValue("Ngày nhập");
            rowheader.CreateCell(4).SetCellValue("Ngày cập nhật");

            int i = 2;
            if (pinCodeList == null)
                return;
            foreach (PinCodeObj pinCode in pinCodeList)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(j).SetCellValue(pinCode.PinCode);
                j++;
                row.CreateCell(j).SetCellValue(pinCode.Serial.ToString());
                j++;
                //row.CreateCell(j).SetCellValue(pinCode.Serial2.ToString());
                //j++;
                row.CreateCell(j).SetCellValue(pinCode.PinCodeStatus.ToString());
                j++;
                row.CreateCell(j).SetCellValue(pinCode.CreatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                row.CreateCell(j).SetCellValue(pinCode.UpdatedDate.AsDateTime().ToString("dd/MM/yyyy"));
                j++;
                i++;
            }
        }

       

        //public ActionResult ProcessExport(PinCodeViewModel model)
        //{
        //    List<Mo> moList = _moDao.GetWithFilter(1000000,1,"",model.FromDate,model.ToDate);
        //    string fileName = "molist_" + Guid.NewGuid().ToString() + ".xls";
        //    ExportToExcel(moList, fileName);
        //    return Redirect("/PinCode");
        //}

        public void ExportToExcel(List<PinCodeObj> cardList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, cardList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }
    }
}
