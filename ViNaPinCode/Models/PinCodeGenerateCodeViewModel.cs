﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class PinCodeGenerateCodeViewModel : Model
    {
        public long ManagerID { get; set; }
        public long ProductTypeID { get; set; }
        public long KeywordID { get; set; }
        public long PinCodeBlockID { get; set; }
        public int PinCodeLength { get; set; }
        public int SerialLength { get; set; }
        public long SubKeyID { get; set; }
        public long TotalPinCodeGenerate { get; set; }
    }
}