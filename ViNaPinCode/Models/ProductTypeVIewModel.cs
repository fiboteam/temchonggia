﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility;
using Vina.DTO;
using VinaVN.Code;

namespace ViNaVN.Models
{
    public class ProductTypeViewModel
    {
        public ProductTypeViewModel()
        {
            ManagerID = -1;
            Page = new Paging() { PageSize = 2 };
            ProductTypeName = "";
            ProductTypeKey = "";
            DataList = new List<ProductType>();
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// MoId của MT
        /// </summary>
        public long ManagerID { get; set; }
        /// <summary>
        /// Control phân trang
        /// </summary>
        /// 
        public ProductTypeStatus ProductTypeStatus { get; set; }

        public Paging Page { get; set; }

        /// <summary>
        /// Số phone
        /// </summary>
        public string ProductTypeName { get; set; }
        public string ProductTypeKey { get; set; }
        /// <summary>
        /// Dữ liệu hiển thị lên viewmodel
        /// </summary>
        public List<ProductType> DataList { get; set; }

    }
}