﻿using Vina.DTO;
using VinaVN.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility;

namespace VinaVN.Models
{
    public class SubKeyViewModel
    {
        public SubKeyViewModel()
        {
            AccountManagerID = -1;
            Page = new Paging() { PageSize = 2 };
            SubKeyValue = "";
            DataList = new List<SubKey>();
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// MoId của MT
        /// </summary>
        public long AccountManagerID { get; set; }
        /// <summary>
        /// Control phân trang
        /// </summary>
        /// 
        public SubKeyStatus SubKeyStatus { get; set; }

        public Paging Page { get; set; }

        /// <summary>
        /// Số phone
        /// </summary>
        public string SubKeyValue { get; set; }
        public long KeywordID { get; set; }

        /// <summary>
        /// Dữ liệu hiển thị lên viewmodel
        /// </summary>
        public List<SubKey> DataList { get; set; }

        public string LoginName { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }

        /// <summary>
        /// Truyền thông tin qua URL IsPopup= true để show thêm khung search hoặc chỉ show Grid dữ liệu
        /// </summary>
        public bool IsPopup { get; set; }
    }
}