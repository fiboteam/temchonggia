﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinaVN.Code;
using Vina.DTO;

namespace VinaVN.Models
{
    public class MtViewModel : Model
    {
        public MtViewModel()
        {
            MoId = 0;
            Page = new Paging() { PageSize = 100};
            Phone = "";
            DataList = new List<Mt>();
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// MoId của MT
        /// </summary>
        public long MoId { get; set; }
        /// <summary>
        /// Control phân trang
        /// </summary>
        public Paging Page { get; set; }

        /// <summary>
        /// Số phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Dữ liệu hiển thị lên viewmodel
        /// </summary>
        public List<Mt> DataList { get; set; }

        /// <summary>
        /// Truyền thông tin qua URL IsPopup= true để show thêm khung search hoặc chỉ show Grid dữ liệu
        /// </summary>
        public bool IsPopup { get; set; }
    }
}