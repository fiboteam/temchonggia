﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Vina.DTO
{
    public class MtConfig : BusinessObject
    {
        public MtConfig()
            : base()
        {
            MtMessage = "";
            MtType = 0;
            AccountManagerID = -1;
        }

        public string MtMessage { get; set; }
        public MtType MtType { get; set; }
        public long AccountManagerID { get; set; }
       
    }
}
