using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class Mo : BusinessObject
    {
        public Mo()
            : base()
        {
            PinCodeID = null;
            ShortCreatedDate = int.Parse(string.Format("{0:yyyyMMdd}", DateTime.Now));
            MoStatus = MoStatus.JustCreated;
        }
        /// <summary>
        /// Mo thuộc mã card nào - nếu có
        /// </summary>
        public long? PinCodeID { get; set; }
        /// <summary>
        /// Phone đã nhắn
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Nội dung tin nhắn
        /// </summary>
        public string MoMessage { get; set; }

        public string MainKey { get; set; }

        public long SubKeyID { get; set; }
        public MoStatus MoStatus { get; set; }
        /// <summary>
        /// Loại Mo - hợp lệ hay k...
        /// </summary>
        /// <summary>
        /// Mã guid chống trùng của tin nhắn
        /// </summary>
        public string GUID { get; set; }
        /// <summary>
        /// Trường dữ liệu tìm kiếm theo ngày
        /// </summary>
        public int ShortCreatedDate { get; set; }
    }

}


