using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class Mt : BusinessObject
    {
        public Mt()
            : base()
        {
            PhoneNumber = "";
            MtMessage = "";
            ShortCreatedDate = int.Parse(string.Format("{0:yyyyMMdd}", DateTime.Now));
        }
        /// <summary>
        /// Mt thuộc Moid nào
        /// </summary>
        public long MoID { get; set; }
        /// <summary>
        /// Số phone gửi ra MT
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Nội dung tin nhắn
        /// </summary>
        public string MtMessage { get; set; }

        public int ShortCreatedDate { get; set; }

    }
}
