using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class PinCodeObj : BusinessObject
    {
        public PinCodeObj()
            : base()
        {
            PinCode = "";
            Serial = "";
            PinCodeStatus = PinCodeStatus.JustCreated;
        }
        /// <summary>
        /// Mã số may mắn
        /// </summary>
        public string PinCode { get; set; }

        public string Serial { get; set; }
        public string Serial2 { get; set; }

        public long AccountManagerID { get; set; }
        public long ProductTypeID { get; set; }

        public long PinCodeBlockID { get; set; }
    
        public PinCodeStatus PinCodeStatus { get; set; }

    }
}
