﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vina.DTO
{
    public class PinCodeTemp : BusinessObject
    {
        public PinCodeTemp()
            : base()
        {
            PinCode = "";
            Serial = "";
        }
        /// <summary>
        /// Mã số may mắn
        /// </summary>
        public string PinCode { get; set; }
        public string Serial { get; set; }
        public long CreatedByID { get; set; }
        public long BlockID { get; set; }
    }
}
